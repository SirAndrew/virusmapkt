package com.example.virusmapkt

import android.location.Location
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.util.*

var imHere: Location? = null // здесь будет всегда доступна самая последняя информация о местоположении пользователя.
val TAG = "KEK"
var StopTimer = false

fun getCoordList(str: String): List<String> {
    var str = str
    //удалить теги и пробелы
    str = str.replace("\\<.*?>".toRegex(), "")
    str = str.replace(" ".toRegex(), "")
    //из большой строки сделать список координатов
    return Arrays.asList(*str.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
}

@Throws(Exception::class)
fun getContentOfHTTPPage(pageAddress: String, codePage: String): String {
    val sb = StringBuilder()
    val pageURL = URL(pageAddress)
    val uc = pageURL.openConnection()
    val br = BufferedReader(
        InputStreamReader(
            uc.getInputStream(), codePage
        )
    )
    try {
        var inputLine = br.readLine()

        while (inputLine != null) {
            sb.append(inputLine)
            inputLine=br.readLine()
        }
    } finally {
        br.close()
    }
    return sb.toString()
}
