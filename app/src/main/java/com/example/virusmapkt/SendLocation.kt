package com.example.virusmapkt

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast



class SendLocation : Service() {
    var uploadCoords: String = ""
    private val CHANNEL_ID = "ForegroundService Kotlin"

    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    override fun onCreate() {
        super.onCreate()

    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        //do heavy work on a background thread
        val input = intent?.getStringExtra("inputExtra")
        createNotificationChannel()
        val notificationIntent = Intent(this, MapsActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Идет отправка данных")
            .setContentText(input)
            .setSmallIcon(R.drawable.corona)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notification)

        StopTimer = false
        StartTimer()

        return START_NOT_STICKY

    }
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }
    companion object {
        fun startService(context: Context, message: String) {
            val startIntent = Intent(context, SendLocation::class.java)
            startIntent.putExtra("inputExtra", message)
            ContextCompat.startForegroundService(context, startIntent)

        }

        fun stopService(context: Context) {
            val stopIntent = Intent(context, SendLocation::class.java)
            context.stopService(stopIntent)
        }
    }

    fun StartTimer (){
        if(!StopTimer) {
            val timer = object : CountDownTimer(20000, 1000) {
                override fun onTick(millisUntilFinished: Long) {}

                override fun onFinish() {
                    var mSendCurrentLocation = SendCurrentLocation()
                    mSendCurrentLocation.execute()
                    StartTimer()
                }
            }
            timer.start()
        }
    }

    override fun onDestroy() {
        Log.d (TAG, "Сервис ФСЁ")
        StopTimer = true
        super.onDestroy()
    }

    inner class SendCurrentLocation : AsyncTask<String, String, String>() {

        override fun doInBackground(vararg strings: String): String? {

            try {
                var lng = imHere?.latitude ?: -1.0
                Log.d(TAG, "$lng")
                var lat = imHere?.longitude ?: 0.0
                if(lng!=-1.0) {
                    uploadCoords = java.lang.Double.toString(lng) + ";" + java.lang.Double.toString(lat)
                    getCoordList(
                        getContentOfHTTPPage(
                            "https://rocky-cove-07551.herokuapp.com/api/addCoord?latlng=$uploadCoords",
                            "utf-8"
                        )
                    )
                    Log.d(TAG, "Загрузка завершена")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }
    }
}
