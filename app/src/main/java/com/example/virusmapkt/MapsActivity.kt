package com.example.virusmapkt


import android.content.pm.PackageManager
import android.graphics.Color
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_maps.*
import java.lang.Double

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    val Volgograd = LatLng(48.7193900, 44.5018300)
    var CoordList: List<String> = listOf()
    var sending = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION),
                11
            )
        }
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 123)
        }

        MyLocationListener.SetUpLocationListener(this)

        button_show.setOnClickListener{
            mMap.clear()
            var mGetList = getList()
            mGetList.execute()
        }

       send_coords.setOnClickListener{
           if(!sending) {
                SendLocation.startService(this, "")
                Toast.makeText(applicationContext, "Передача данных началась", LENGTH_SHORT ).show()
                sending = true
           }
           else{
                SendLocation.stopService(this)
                Toast.makeText(applicationContext, "Передача данных закончилась", LENGTH_SHORT ).show()
                sending = false
           }
       }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Volgograd))
        val camPos = CameraPosition.Builder()
            .target(Volgograd)
            .zoom(10f)
            .bearing(0f)
            .tilt(0f)
            .build()
        val camUpd3 = CameraUpdateFactory.newCameraPosition(camPos)
        googleMap.animateCamera(camUpd3)
    }

    fun drowCircles(coordList: List<String>) {
        try {
            for (i in coordList.indices) {
                Log.d(TAG, coordList[i])
                mMap.addCircle(getCircleOptions(coordConvert(coordList[i])))
            }
        } catch (e: NullPointerException) {
            Log.d(TAG, "Ошибка отрисовки")
        }
    }

    fun coordConvert(coord: String): LatLng {
        val pos = coord.indexOf(";")
        return LatLng(Double.valueOf(coord.substring(0, pos)), Double.valueOf(coord.substring(pos + 1)))
    }

    fun getCircleOptions(Coord: LatLng): CircleOptions {
        val co = CircleOptions()
        co.center(Coord)
        co.radius(1.0)
        co.fillColor(Color.RED)
        co.strokeColor(Color.RED)
        co.strokeWidth(2.0f)
        return co
    }

    internal inner class getList : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg params: Void?): String? {
            try {
                CoordList = getCoordList(
                    getContentOfHTTPPage(
                        "https://rocky-cove-07551.herokuapp.com/api/allcoords",
                        "utf-8"
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            drowCircles(CoordList)
        }
    }
}
